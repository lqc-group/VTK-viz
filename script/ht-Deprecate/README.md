# README File for Batch Submission

Author: Jacob Zorn  
Version: 1.0  
Date Uploaded: 11/27/2017 

## Batch Creation and Submission of Jobs  
**Script Name:** BatchSubmit.sh  
**Script Call:** ./BatchSubmit.sh  
**Description of BatchSubmit.sh:**

You can use this script to handle the batch creation of directories, input files,
pot files, pbs files, along with the submission of those pbs files to the open queue.
The script is easily adjustable to fit whatever parameters you are trying to change. 
Currently, the script only works for creating two levels of directories, however,
extension to three or four levels or the retraction to one level is straight forward
if necessary for your work. The script is pretty well commented to help you walk
through the script and understand how it works. This script will create directories
inside the directory that this scripts sits in. Also, this script assumes that 
each of the strain components are the same (i.e. e11 = e22 = e12), a further script
is under development to allow for non equal strain values.
Example: If this script is located at ~/Path/BatchSubmit.sh, the directories this
script produces are found at ~/Path/NewFolder1 and ~/Path/NewFolder1/SubFolder1, etc.
depending on how many levels you set the script to run.

## Batch Visualization using FerroDomainVTK
**ScriptName:** FerroDomain.sh  
**Script Call:** ./FerroDomain.sh  
**Description of FerroDomain.sh:**

You can use this script to handle the batch visualization of .dat files for understanding
the domain structures of your jobs. This script does not produce any new directories and
instead just works in and out of previously created directories. Like BatchSubmit.sh
this script only works for data structures at are two levels deep. However, the extension 
or retraction is relatively straight forward and easy. The script is pretty well commented
to help you walk through the script. You will need to place a visualPolar.in file in the 
same directories as this script. Additionally, the parameters in the visualPolar.in file
should be set so you can achieve the necessary correct images and/or analytics. To run 
this, make sure the FerroDomainVTK program is set up in your .bashrc files so you have
an appropriate alias.

## Batch Job Submission
**Script Name:** JobSubmit.sh  
**Script Call:** ./JobSubmit.sh  
**Description of JobSubmit.sh:**

You can use this script to submit a number of jobs quickly. This script only runs one
level deep, but can be easily extended if necessary. The .pbs files you will want to
submit have to filled out correctly and inside a sub-directory(s) of the directory 
this script is located in. The script is pretty well commented to help you walk through
the script and understand it.

## General batch job submission functions
**Script Name:** general-batch.sh  
**Script Usage:** ```source genreal-batch.sh```and then use individual function as normal bash commands. Should add the source command to .bashrc file for future easy access.  
**Script Description:**

Several functions are created for the purpose of generating arbitrary layers of folders (each layer represent
batch replacement of one variable).

### Contained functions:
1. batchFolderOld: generate arbitrary layer of folders, and replace the keyword (same as folder names)
with a number. This command is suitable for both the fixed format and free format input file.
2. batchFolderNew: generate arbitrary layer of folders, and replace the whole line that contains the keyword (same as folder names)
with the variables, the benefit of this command compare with the old one is you can replace multiple numbers at the same time. This 
command is only suitable for the free format input file.
3. batchCommand: go into specified subfolder and execute the command, the folder syntax is the same as batchFolderNew/Old
4. batchAllCommand: go into all subfolder rather than specified ones.

The following functions are utility functions used by the previous four.
1. getFirstString: find the first position of the specified deliminator, separate the string into two parts and return the first part. 
2. getRemainString: find the first position of the specified deliminator, separate the string into two parts and return the second part.
3. copyFileReplace: copy specified file from the .. folder, and replace the keyword in the file with a new number
4. copyFileReplaceWholeLine: copy specified file from the .. folder, find the line that contains the keyword, and replace the part after "=" with new variable.
5. stdOutWithTab: change the terminal output style, add several tab before standard output, used for better display of the verbose print out information.
6. restoreStdOut: restore the terminal output style to normal.

### Function Usage
##### batchFolderOld [$1:folder\_structure\_string] [$2:copy\_file\_list] [$3:replacement\_variable]  
- $1: Use "#" as the separator between folder layers, e.g. "mystrain#mytemp"
- $2: Use " " as the separator between files, add "&" before the file name you want to be processed, and nothing for files just copy but not processed e.g. "&input.in ferro.pbs"
- $3: Use "#" as the separator between different variable, and " " as separator for one variable but different values, e.g. "-0.1 0 0.1#200 250 300" 

##### batchFolderNew [$1:folder\_structure\_string] [$2:copy\_file\_list] [$3:replacement\_variable]  
- $1: Use "#" as the separator between folder layers, e.g. "SYSDIM#TEM"
- $2: Use " " as the separator between files,  add "&" before the file name you want to be processed, and nothing for files just copy but not processed e.g. "&input.in ferro.pbs"
- $3: Use "#" as the separator between different variable, and " " as separator for one variable but different values, then "|" as separator for different numbers 
 for one replacement (since it's replacing the whole line, there may be more than more numbers) e.g. "100|100|100 200|200|200#200 250 300" 

##### batchCommand [$1:folder\_structure\_string] [$2:replacement\_variable] [$3:command\_string]  
- $1: Use "#" as the separator between folder layers, e.g. "mystrain#mytemp"
- $2: Use "#" as the separator between different variable, and " " as separator for one variable but different values, e.g. "-0.1 0 0.1#200 250 300" 
- $3: A command in the form of string, that is the words you would type in terminal to execute the command, e.g. "qsub ferro.pbs"

##### batchAllCommand [$1:command\_string]  
- $1: A command in the form of string, that is the words you would type in terminal to execute the command, e.g. "qsub ferro.pbs"

### Example Usage
``` 
folders="SYSDIM#REALDIM"            # separated by "#"
files="&input.in pot.in ferro.pbs"   # separated by " ", use "&" for file to be processed
sysdim="100|200|300 400|500|600"    # separated by " " for different variable values, and "|" for different numbers in one replacement
realdim="100|200|300 400|500|600"   # same as above
variables="${sysdim}#${realdim}"    # separated by "#" for different keywords
batchFolderNew "${folders}" "${files}" "${variables}"
batchCommand "${folders}" "${variables}" "qsub ferro.pbs"
```
```
folders="mystrain#mytemp"           # separated by "#"
files="&input.in pot.in ferro.pbs"   # separated by " ", use "&" for file to be processed
strain="-0.2 -0.1 0 0.1 0.2"        # separated by " " for different variable values,
temp="200 250 300 350 400"          # same as above
variables="${strain}#${temp}"       # separated by "#" for different keywords
batchFolderOld "${folders}" "${files}" "${variables}"
batchCommand "${folders}" "${variables}" "qsub ferro.pbs"
```
## Updates to Project Files:
* 11/28/2017: Uploaded new BatchSubmit.sh file that is commented as necessary.  
* 11/28/2017: Uploaded new FerroDomain.sh file that is commented as necessary.  
* 11/28/2017: Uploaded new JobSubmit.sh file that is commented as necessary.  
* 12/04/2017: Uploaded correct input.in file, with necessary comments and variables
* 12/11/2017: Add general-batch.sh, it contains command for more general folder structure generator and job submission

## Updates to README file (Updater):
* 11/28/2017: Added Description of BatchSubmit.sh (JZ)  
* 11/28/2017: Added Description of FerroDomain.sh (JZ)  
* 11/28/2017: Added Description of JobSubmit.sh (JZ)  
* 12/04/2017: Addressed possibility of using unequal strains in the Batch Creation Script
* 12/11/2017: doc for the functions in the general-batch.sh script