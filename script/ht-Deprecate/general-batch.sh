#!/bin/bash


function getFirstString(){
	# This function separate the string into two parts
	# input: $1: separator, $2:string
	# return: first string before the first separator

	local separator=$2
	local parse_string=$1
	local index=0

	IFS=" "
	if [ ! -z "$2" ]; then
		IFS=$separator
	fi
	# echo "|"$IFS"|"
	local folder_list=(${parse_string}) # or use this one read -ra folder_list<<<"$parse_string"
	IFS=" "
	echo ${folder_list[0]}

}

function getRemainString(){
	# This function separate the string into two parts
	# input: $1: separator, $2:string
	# return: the remaining string after the first separator

	local separator=$2
	local parse_string=$1
	local output=( "" "")
	local index=0

	IFS=" "
	if [ ! -z "$2" ]; then
		IFS=$separator
	fi
	# echo "|"$IFS"|"
	local folder_list=(${parse_string}) # or use this one read -ra folder_list<<<"$parse_string"
	IFS=" "
	for i in "${folder_list[@]}"; do
		if [ "$index" = "0" ] || [ "$index" = "1" ]; then
			#statements
			output[${index}]=$i
		else
			output[1]=${output[1]}${separator}${i}
		fi
		let index=($index+1)
	done
	echo ${output[1]}
}

function copyFileReplace(){
	# copy file from .. to current, and
	# replace the keyword
	# with the variable
	# input: 1. folder name (keyword), 2.filename 3. variable value
	# if no variable value is given then simply copy the file
	local folder=$1
	local copyFile=$2
	local variable=$3

	local variable_name=$(getFirstString "${folder}" "_")
	local variable_space=$(exec echo "${variable}"|sed "s/|/_/g")

	cp ../$copyFile .
	if [ ! -z "$3" ]; then
		# echo ${variable_name} ${variable} ${copyFile}
		# sed -i '.rpltmp' "s/${variable_name}/${variable}/g" ${copyFile} # this is for on mac
		sed -i "s/${variable_name}/${variable_space}/g" ${copyFile}
		# rm *.rpltmp # this is for on mac
	fi
}

function copyFileReplaceWholeLine(){
	# copy file from .. to current, and
	# replace the keyword
	# with the variable
	# input: 1. folder name (keyword), 2.filename 3. variable value
	# if no variable value is given then simply copy the file
	local folder=$1
	local copyFile=$2
	local variable="$3"

	local variable_name=$(getFirstString "${folder}" "_")
	local variable_space=$(exec echo "${variable}"|sed "s/|/ /g")
	cp ../$copyFile .
	if [ ! -z "$3" ]; then
		# echo ${variable_name} ${variable} ${copyFile}
		#sed -i '.rpltmp' "/${variable_name}/c ${variable_name} = ${variable_space}" ${copyFile} # this is for on mac
		sed -i "/${variable_name}/c ${variable_name} = ${variable_space}" ${copyFile}
		# rm *.rpltmp # this is for on mac
	fi
}

function stdOutWithTab(){
	# redirect the command output through paste command that add
	# certain level of tab before
	# input:1 .tab levels 2. the tmp file location
	local level=$1
	local tab_string=""
	for (( i = 0; i < ${level}; i++ )); do
		tab_string=${tab_string}$'\t'
	done
	exec 3>&1
	exec 1> >(sed -e $"s/^/${tab_string}/g" >&3)
}

function restoreStdOut(){
	# restore the normal command output behaviour
	exec 1>&3 3>&-
}

function batchFolder(){
	# This is the function to do the whole job of batch job submition
    # the difference between this function and the batchfoldernew and batchfolderold
    # is this function is a combination of both new and old replace type
    # new type is indicated by & and the olde type is indicated by #.
    # This function is inspired by this use case, when we copy and replace the new mupro free format input
    # suppose we also want to change and replace the qsub name in pbs file, which request
    # an old type rather than new type replacement
	# input: 1. string that gives the folder structure separated by "#"
	# for example TEM#MISMATCH#GRAD, the folder name is also the keyword for replacement
	# 2. a string the gives the file list to be copied into the generated folders
	# separated by space, for example "&input.in ferro.pbs" and "&" before the filename
    # you want to change using the new format, which replace the whole line, and "#" before the
    # filename you want to change in the old style
	# 3. a string that store the variable list for keyword replacement, use "#" as
	# separater for different variables and space as separator for the variable array
	# for example, "298 350 400#0.1 0.2 -0.1 -0.2#0.6 0.3 0.15", notice the sequence of
	# variables must be the same as the folder structure string
	# A full example of usage would be as follows:
	# folders="SYSDIM#REALDIM"
	# files="&input.in pot.in ferro.pbs"
	# sysdim="1|2|3 4|5|6"
	# realdim="123|234|234 456|567|678"
	# variables="${sysdim}#${realdim}"
	# batchFolderNew "${folders}" "${files}" "${variables}"

	local folder_string=$1
	local file_string=$2
	local variable_string=$3


	local file_list=($file_string)
	local batch_depth=0
	local batch_depth_plus=1
	local folder_create=$(getFirstString "${folder_string}" "#")
	local folder_pass_on=$(getRemainString "${folder_string}" "#")
	local variable_current=($(getFirstString "${variable_string}" "#"))
	local variable_pass_on=$(getRemainString "${variable_string}" "#")

	if [ ! -z "$4" ]; then
		let batch_depth=$4
	fi
	stdOutWithTab ${batch_depth}
	echo -e "Going to create ${folder_create} series."
	for child in "${variable_current[@]}";do
		#statements
		local child_underscore=$(exec echo "${child}"|sed "s/|/_/g")
		local child_folder="${folder_create}_${child_underscore}"
		echo -e "Child folder: ${child_folder}"
		if [ ! -d "${child_folder}" ]; then
			#statements
			mkdir ${child_folder}
		fi
		cd ${child_folder}

		for file in "${file_list[@]}"; do
			if [[ ${file:0:1} == "&" ]]; then
				copyFileReplaceWholeLine "${child_folder}" "${file:1}" "${child}"
			elif [[ ${file:0:1} == "#" ]]; then
                copyFileReplace "${child_folder}" "${file:1}" "${child}"
            else
				copyFileReplaceWholeLine "${child_folder}" "${file}"
			fi
		done

		if [ ! -z "${folder_pass_on}" ]; then
			let batch_depth_plus=(${batch_depth}+1)
			restoreStdOut
			batchFolder "${folder_pass_on}" "${file_string}" "${variable_pass_on}" ${batch_depth_plus}
			stdOutWithTab ${batch_depth}
 		fi

		cd ..
	done
	echo -e "Finished ${folder_create} series."
	restoreStdOut
	sleep 0.1
}




function batchFolderNew(){
	# This is the function to do the whole job of batch job submition
	# input: 1. string that gives the folder structure separated by "#"
	# for example TEM#MISMATCH#GRAD, the folder name is also the keyword for replacement
	# 2. a string the gives the file list to be copied into the generated folders
	# separated by space, for example "&input.in ferro.pbs" and "&" before the filename you want to change
	# 3. a string that store the variable list for keyword replacement, use "#" as
	# separater for different variables and space as separator for the variable array
	# for example, "298 350 400#0.1 0.2 -0.1 -0.2#0.6 0.3 0.15", notice the sequence of
	# variables must be the same as the folder structure string
	# A full example of usage would be as follows:
	# folders="SYSDIM#REALDIM"
	# files="&input.in pot.in ferro.pbs"
	# sysdim="1|2|3 4|5|6"
	# realdim="123|234|234 456|567|678"
	# variables="${sysdim}#${realdim}"
	# batchFolderNew "${folders}" "${files}" "${variables}"

	local folder_string=$1
	local file_string=$2
	local variable_string=$3


	local file_list=($file_string)
	local batch_depth=0
	local batch_depth_plus=1
	local folder_create=$(getFirstString "${folder_string}" "#")
	local folder_pass_on=$(getRemainString "${folder_string}" "#")
	local variable_current=($(getFirstString "${variable_string}" "#"))
	local variable_pass_on=$(getRemainString "${variable_string}" "#")

	if [ ! -z "$4" ]; then
		let batch_depth=$4
	fi
	stdOutWithTab ${batch_depth}
	echo -e "Going to create ${folder_create} series."
	for child in "${variable_current[@]}";do
		#statements
		local child_underscore=$(exec echo "${child}"|sed "s/|/_/g")
		local child_folder="${folder_create}_${child_underscore}"
		echo -e "Child folder: ${child_folder}"
		if [ ! -d "${child_folder}" ]; then
			#statements
			mkdir ${child_folder}
		fi
		cd ${child_folder}

		for file in "${file_list[@]}"; do
			if [[ ${file:0:1} == "&" ]]; then
				copyFileReplaceWholeLine "${child_folder}" "${file:1}" "${child}"
			else
				copyFileReplaceWholeLine "${child_folder}" "${file}"
			fi
		done

		if [ ! -z "${folder_pass_on}" ]; then
			let batch_depth_plus=(${batch_depth}+1)
			restoreStdOut
			batchFolderNew "${folder_pass_on}" "${file_string}" "${variable_pass_on}" ${batch_depth_plus}
			stdOutWithTab ${batch_depth}
 		fi

		cd ..
	done
	echo -e "Finished ${folder_create} series."
	restoreStdOut
	sleep 0.1
}

function batchFolderOld(){
	# This is the function to do the whole job of batch job submition
	# input: 1. string that gives the folder structure separated by "#"
	# for example TEM#MISMATCH#GRAD, the folder name is also the keyword for replacement
	# 2. a string the gives the file list to be copied into the generated folders
	# separated by space, for example "&input.in ferro.pbs" and "&" before the filename you want to change
	# 3. a string that store the variable list for keyword replacement, use "#" as
	# separater for different variables and space as separator for the variable array
	# for example, "298 350 400#0.1 0.2 -0.1 -0.2#0.6 0.3 0.15", notice the sequence of
	# variables must be the same as the folder structure string
	# A full example of usage would be as follows:
	# folders="mystrain#mytemp"
	# files="&input.in pot.in ferro.pbs"
	# strain="-0.2 -0.1 0 0.1 0.2"
	# temp="200 250 300 350 400"
	# variables="${strain}#${temp}"
	# batchFolderOld "${folders}" "${files}" "${variables}"

	local folder_string=$1
	local file_string=$2
	local variable_string=$3


	local file_list=($file_string)
	local batch_depth=0
	local batch_depth_plus=1
	local folder_create=$(getFirstString "${folder_string}" "#")
	local folder_pass_on=$(getRemainString "${folder_string}" "#")
	local variable_current=($(getFirstString "${variable_string}" "#"))
	local variable_pass_on=$(getRemainString "${variable_string}" "#")

	if [ ! -z "$4" ]; then
		let batch_depth=$4
	fi
	stdOutWithTab ${batch_depth}
	echo -e "Going to create ${folder_create} series."
	for child in "${variable_current[@]}";do
		#statements
		local child_folder="${folder_create}_${child}"
		echo -e "Child folder: ${child_folder}"
		if [ ! -d "${child_folder}" ]; then
			#statements
			mkdir ${child_folder}
		fi
		cd ${child_folder}

		for file in "${file_list[@]}"; do
			if [[ ${file:0:1} == "&" ]]; then
				copyFileReplace "${child_folder}" "${file:1}" "${child}"
			else
				copyFileReplace "${child_folder}" "${file}"
			fi
		done

		if [ ! -z "${folder_pass_on}" ]; then
			let batch_depth_plus=(${batch_depth}+1)
			restoreStdOut
			batchFolderOld "${folder_pass_on}" "${file_string}" "${variable_pass_on}" ${batch_depth_plus}
			stdOutWithTab ${batch_depth}
 		fi

		cd ..
	done
	echo -e "Finished ${folder_create} series."
	restoreStdOut
	sleep 0.1
}
#${tab_string}


function batchCommand(){
	# for batch job submission, but can also be used to any other commands
	# input: 1. folder structure the same as in batchFolderOld
	# 2. variables string, the same as in batchFolderOld arguement 3
	# 3. the command in string that you want to execute inside each folder
	local folder_string=$1
	local variable_string=$2
	local command_string_origin="$3"


	local batch_depth=0
	local batch_depth_plus=1
	local folder_create=$(getFirstString "${folder_string}" "#")
	local folder_pass_on=$(getRemainString "${folder_string}" "#")
	local variable_current=($(getFirstString "${variable_string}" "#"))
	local variable_pass_on=$(getRemainString "${variable_string}" "#")

	if [ ! -z "$4" ]; then
		let batch_depth=$4
	fi
	let batch_depth_plus=(${batch_depth}+1)
	stdOutWithTab ${batch_depth}
	echo -e "Going into ${folder_create} series."
	for child in "${variable_current[@]}";do
		#statements
		local child_underscore=$(exec echo "${child}"|sed "s/|/_/g")
		local child_folder="${folder_create}_${child_underscore}"
		echo -e "Child folder: ${child_folder}"
		cd ${child_folder}
        command_string=$(echo ${command_string_origin} | sed "s/\$${folder_create}/${child_underscore}/g")

		if [ ! -z "${folder_pass_on}" ]; then
            command_string=$(echo ${command_string} | sed "s/\$VAR/${child_folder}_\$VAR/g")
			restoreStdOut
			batchCommand "${folder_pass_on}" "${variable_pass_on}" "${command_string}" ${batch_depth_plus}
			stdOutWithTab ${batch_depth}
		else
            command_string=$(echo ${command_string} | sed "s/\$VAR/${child_folder}/g")
			restoreStdOut
			stdOutWithTab ${batch_depth_plus}
			echo "Executing command ${command_string}"
			eval "${command_string}"
			restoreStdOut
			stdOutWithTab ${batch_depth}
		fi

		cd ..
	done
	echo -e "Finished ${folder_create} series."
	restoreStdOut
	sleep 0.1
}

function batchAllCommand(){
	# This command will loop through all folders and execute a command
	# input: 1. the command you want to execute
	# 2. is optional integer that tune the indentation before output

	local command_string="$1"
	local batch_depth=0
	local batch_depth_plus=1
	local folder_array=($(find . -maxdepth 1 -type d))
	local current_dir=$(eval pwd)
	local dir=""
	if [ ! -z "$2" ]; then
		let batch_depth=$2
	fi
	stdOutWithTab ${batch_depth}
	if [ ${#folder_array[@]} = 1 ]; then
		echo "Executing ${command_string}"
		eval "${command_string}"
	fi
	for dir in */ ; do
		if [[ -d ${dir} ]]; then
			echo "Going into ${dir}, from ${current_dir}"
			cd  ${dir}
			let batch_depth_plus=(${batch_depth}+1)
			restoreStdOut
			batchAllCommand "${command_string}" ${batch_depth_plus}
			stdOutWithTab ${batch_depth}
			cd ..
			echo "Return from ${dir}, to ${current_dir}"
		fi
	done

	restoreStdOut
	sleep 0.1
}
