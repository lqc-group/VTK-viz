#! /bin/bash
#Batch Submission of Jobs
#Written by Jacob Zorn
#11/1/2017
#Batch script that is useful in the submission of jobs onto the server. This script needs to be placed
#in a parent directory. I.e. A directory has sub-directories [say N01 N02 N03 etc.] and your .pbs files 
#are in each sub-directory. Place this script in the parent directory that contains the sub-directories
#then run the script to submit the jobs. Note the pbs files need to be filled out correctly.

#Loop through a given vector of directories
for i in N09 N05 N04 N03 N02 N01 P00 P01 P02 P03 P04 P05 P09
do
cd $i
#Inside each directory submit the .pbs file.
qsub ferro.pbs
cd ..
done

